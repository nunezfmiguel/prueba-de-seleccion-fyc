
La siguiente Tarea tiene el fin de evaluar conceptos básicos de desarrollo y uso de buenas prácticas en el ámbito de la programación. Las personas que tendrán acceso a este repositorio tiene como fecha máxima de entrega el día Miercoles 15 de Enero del 2020 hasta las 23:59 horas, para ser evaluados correctamente. De lo contrario no serán considerados en el proceso de evaluación.

# Contenidos de la Evaluación.

* Seguimientos de instrucciones.
* Resolución de problemas básico de programación.
* Análisis y manejo de objetos.
* Algoritmos simples de ordenamiento y búsqueda o Uso de métodos complejos del lenguaje.


## Pre-requisitos de instalación

#### Node.js

Para crear el ambiente a desarrollar es necesario instalar  [Node.js Website](https://nodejs.org/), te recomendamos la instalación LTS. Es importante que en la instalación de Node.js instales el gestor de paquetes de Node - [npm](https://www.npmjs.com/), y que en la instalación se generen la configuración de variables de entorno en tu sistema operativo.
<p align="center">
  <a href="https://nodejs.org/">
    <img
      alt="Node.js"
      src="https://nodejs.org/static/images/logo-light.svg"
      width="200"
    />
  </a>
</p>

Luego de instalar al abrir la consola de tu sistema operativo. Puedes escribir el siguiente comando.

```console
node -v
```

La salida seria algo como esto:
```console
v12.14.1
```

Comprueba que el gestor de paquetes de node este instalado correctamente.

```console
npm -v
```

Su salida es :
```console
6.1.0
```

Puedes aprender mas sobre Node.js en su [documentacion](https://nodejs.org/dist/latest-v12.x/docs/api/).

#### JSON Server

Para el siguiente ejercicio necesitamos montar un servidor web que pueda responder nuestras solicitudes, un rápido y útil REST API es  [JSON Server](https://github.com/typicode/json-server).
Solo debes escribir el siguiente comando en un directorio de trabajo por ejemplo C:/User/<mi_user>/Documents/proyectos/

```console
npm install -g json-server
```

ya generaste la instalación de forma global de este paquete de node, solo queda levantar el web-server, dentro del repositorio que puedes clonar o descargar encontrar un archivo "db.json", si ejecutas el siguiente comando se levantan las siguientes rutas.

```console
json-server --watch db.json
```

 * http://localhost:3000/productos_catalogo
 * http://localhost:3000/productos
 * http://localhost:3000/eventos
 * http://localhost:3000/imagenes
 

## Solicitud a desarrollar

Queremos que consumas estos endpoint y que nos entregues un listado de las siguientes solicitudes. Puedes imprimirlos directamente en consola o si tus capacidades y tiempo te lo permiten mostrarlos en el navegador en un html. Ningún lenguaje es excluyente de evaluación, aunque nosotros te recomendamos Node.js o [Typescript](https://www.typescriptlang.org/).


-	Listados de productos que se encuentren “offline”.
-	Listado de productos que estén asociados algún evento y estén “online”.
-	Productos “importados” que no contengan “imágenes”.
-   Todos nuestros productos ordenados por departamento de menor a mayor.

## Como entregar tu respuesta

Para entregar tu respuesta tienes 3 opciones:
 * Desde este mismo repositorio generando un merge request - te recomendamos generar un branch con tu nombre y apellido ejemplo "carlos-gonzalez".
    - para poder generar un merger-request, lo primero que debes hacer es un fork del repositorio, esta imagen indica donde debes ir: 
    
    <p align="center">
      <a href="#">
        <img
          alt="Node.js"
          src="https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_fork_button.png"
          width="600"
        />
      </a>
    </p>
    
    - luego aparece una pantalla con esta:
  
     <p align="center">
          <a href="#">
            <img
              alt="Node.js"
              src="https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_choose_namespace.png"
              width="600"
            />
          </a>
        </p>
        
     - solo debes seleccionar tu usuario y crearas un nuevo fork dentro de tu cuenta de gitlab
     
     - Ahora solo resta desarrollar y generar un merge-request desde tu fork. Se solicita que generes un branch al momento de enviar tu merge-request
  
 
 * Creando otro repositorio publico en Gitlab o Github adjuntando el link del repo como respuesta del mail que ha llegado.
 * comprimiendo su respuesta agregando el nombre y apellido puedes estar en la comprecion que estimos para tu sistema operativo (zip, rar, tgz). 




### Ayudas - Sugerencias

Existen herramientas que te permiten consumir de forma simple estos recursos, [Postman](https://www.getpostman.com/) es una de ellas. Si estas desarrollando esta solicitud es que fuiste preseleccionado para el puesto de Analista desarrollador en Cencosud S.A. - Área Catalogo y Fotografia. Te recomendamos repasar conceptos de programación básica en:

* Node.js
* Java 1.8+
* SQL
* Mongodb
* SpringBoot
* Angular 7+ 
* Docker
* Kubernetes
* Jenkins

Parece un gran "pool" de tecnologías, las que podrás ir perfeccionando en el camino que construyas en esta empresa.

Mucha suerte DEV :muscle: :muscle: :muscle: , Esperamos tu respuesta










